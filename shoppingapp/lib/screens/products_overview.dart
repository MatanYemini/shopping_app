import 'package:flutter/material.dart';
import '../models/product.dart';
import '../widgets/product_item.dart';

class ProductsOverviewScreen extends StatelessWidget {
  final Set<Product> loadedProducts = {
    Product(
        id: '1',
        title: 'try',
        description: 'try try',
        price: 12.5,
        imageUrl:
            'https://www.google.com/search?q=fun&sxsrf=ACYBGNQ8h2VNamOq-N6W0IFdUSwFQbgiqw:1574537409147&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiJttT3iIHmAhVOblAKHSA_DaMQ_AUoAXoECBEQAw&biw=562&bih=615#imgrc=Uk7T7Jcox93QVM:',
        isFavorite: false),
    Product(
        id: '1',
        title: 'try',
        description: 'try try',
        price: 12.5,
        imageUrl:
            'https://www.google.com/search?q=fun&sxsrf=ACYBGNQ8h2VNamOq-N6W0IFdUSwFQbgiqw:1574537409147&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiJttT3iIHmAhVOblAKHSA_DaMQ_AUoAXoECBEQAw&biw=562&bih=615#imgrc=Uk7T7Jcox93QVM:',
        isFavorite: false)
  };

  List<Product> setToList(Set<Product> setProds) {
    return setProds.toList();
  }

  @override
  Widget build(BuildContext context) {
    final List<Product>lstProds = setToList(loadedProducts);
    return Scaffold(
      appBar: AppBar(
        title: Text('ShopShit'),
      ),
      body: GridView.builder(
        padding: const EdgeInsets.all(10.0),
        itemCount: loadedProducts.length,
        itemBuilder: (ctx, i) => ProductItem(lstProds[i]),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
      ),
    );
  }
}
