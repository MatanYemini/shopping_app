import 'package:flutter/material.dart';
import '../models/product.dart';

class ProductItem extends StatelessWidget {
  final Product productItem;

  ProductItem(this.productItem);

  @override
  Widget build(BuildContext context) {
    return GridTile(
      child: Image.network(productItem.imageUrl, fit: BoxFit.cover),
      footer: GridTileBar(
        backgroundColor: Colors.black54,
        leading: IconButton(icon: Icon(Icons.favorite), onPressed: () {},),
        title: Text(
          productItem.title,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
